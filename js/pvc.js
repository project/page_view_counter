(function($, Drupal) {
    Drupal.behaviors.pvc_ajax = {
    attach: function (context, settings) {
      const PVC_ONCE = once('pvc_ajax', 'body', context);
      const tstamp = new Date();
      $(PVC_ONCE).each(() => {
        $.ajax({
          url: settings.pvc.url + `&t=${tstamp.getTime()}`
        }).done((count) => {
          $(`#${settings.pvc.element_id}`).html(count.toLocaleString());
        });
      })
    }
  }
}(jQuery, Drupal));
