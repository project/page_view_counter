<?php

namespace Drupal\page_view_counter\Service;

use Drupal\Core\State\State;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\page_view_counter\PageViewCounterManager;
use Drupal\Core\Routing\CurrentRouteMatch;
class PVCCookieService {

  /**
   * {@inheritdoc}
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  protected $routeMatch;
  /**
   * Constructs PVCCookieService.
   *
   * @param Symfony\Component\HttpFoundation\RequestStack;
   *  The request.
   *
   * @param Drupal\Core\State\State;
   *. The state.
   *
   */
  public function __construct(RequestStack $request, CurrentRouteMatch $routeMatch, State $state){
    $this->request = $request;
    $this->routeMatch = $routeMatch;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function procesPVCCookie($cookies = [], $route_id = '') {
    $validCookies = $this->detectValidCookies($cookies);
    $timestamp = !empty($validCookies[$route_id]) ? $validCookies[$route_id] : 0;
    return $this->processCount($route_id, $timestamp);
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteID() {
    $rawParams = $this->routeMatch->getRawParameters()->all();
    return $this->buildKey($rawParams);
  }

  /**
   * {@inheritdoc}
   */
  protected function detectValidCookies($cookies) {
    $prefix = PageViewCounterManager::COOKIE_PREFIX;
    $valid_cookies = [];
    foreach ($cookies as $key => $value) {
      if (str_contains($key, $prefix)) {
        $valid_cookies[$key] = $value;
      }
    }
    return $valid_cookies;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMaxAge() {
    $time = \Drupal::service('config.factory')->getEditable('page_view_counter.settings')->get('time');
    if (empty($time)) {
      return PageViewCounterManager::DEFAULT_MAX_AGE;
    }
    return $time;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildKey($rawParams) {
    if (empty($rawParams)) {
      return FALSE;
    }
    $id = PageViewCounterManager::COOKIE_PREFIX;
    foreach ($rawParams as $key => $rawParam) {
      $id .= '_' . $key . '_' . $rawParam;
    }
    return $id;
  }

  /**
   * {@inheritdoc}
   */
  protected function processCount($route_id, $timestamp) {
    $count = 1;
    if ($this->state->get($route_id)) {
      if ((time() - $timestamp) > $this->getMaxAge() || empty($timestamp)) {
        $count = intval($this->state->get($route_id)) + 1;
        $this->state->set($route_id, $count);
        $timestamp = time();
      }
      $count = $this->state->get($route_id);
    } else {
      $timestamp = time();
      $this->state->set($route_id, $count);
    }
    return [
      'count' => $count,
      'timestamp' => $timestamp,
      'expire' => $this->getMaxAge(),
    ];
  }
}
