<?php

namespace Drupal\page_view_counter\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\page_view_counter\PageViewCounterManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Returns responses for page view counter routes.
 */
class PageViewCounterController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request) {
    $this->request = $request->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Builds the response.
   */
  public function fetch() {
    $cookies  = $this->request->cookies->all();
    $query = $this->request->query->all();
    $route_id = !empty($query[PageViewCounterManager::QUERY_KEY]) ? $query[PageViewCounterManager::QUERY_KEY] : FALSE;
    if (empty($route_id)) {
      return $this->getCachedResponse(PageViewCounterManager::COOKIE_PREFIX, time(), 0);
    }
    $result = \Drupal::service('page_view_counter.cookie_service')->procesPVCCookie($cookies, $route_id);
    return $this->getCachedResponse($route_id, $result);
  }

  /**
   * Cached API Requests / Expires every x minutes.
   */
  protected function getCachedResponse($route_id, $result) {
    $time = $result['timestamp'];
    $view_count = $result['count'];
    $expire = $result['expire'];
    $metadata = new CacheableMetadata();
    $metadata->setCacheContexts(['url.query_args']);
    $response = new CacheableJsonResponse($view_count);
    $response->addCacheableDependency($metadata);
    $response->setPublic();
    $response->setMaxAge(5);
    $response->headers->setCookie(new Cookie($route_id, $time, $time + $expire));
    return $response;
  }
}
