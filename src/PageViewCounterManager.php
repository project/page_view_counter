<?php

namespace Drupal\page_view_counter;

class PageViewCounterManager {

  const ELEMENT_ID = 'pvc_count';

  const COOKIE_PREFIX = 'pvc_';

  const QUERY_KEY = 'route_id';

  const DEFAULT_MAX_AGE = 60;
}
